Nombre: María Victoria Gago Riveros

Requisitos mínimos:

- Método constant
- Método square
- Método show
- Método info
- Método shift
- Método trim
- Método repeat
- Programa sound.py
- Programa menu.py

Requisitos opcionales:

- Método clean
- Método round
- Programa menu2.py
- Programa menu3.py
- Método add