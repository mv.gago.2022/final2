"""Methods for processing sound (sound processors)"""

import config


def ampli(sound, factor: float):
    """Amplify signal (sound) by the specified factor

    :param sound:  sound (list of samples) to amplify
    :param factor: amplification factor
    :return:     list of samples (sound)
    """

    for nsample in range(len(sound)):
        value = sound[nsample] * factor
        if value > config.max_amp:
            value = config.max_amp
        elif value < - config.max_amp:
            value = - config.max_amp
        sound[nsample] = int(value)

    return sound


def shift(sound, value: int):  # Método que desplaza el sonido (suma)

    for nsample in range(len(sound)):

        desplazamiento = sound[nsample] + value

        if desplazamiento > config.max_amp:
            desplazamiento = config.max_amp

        elif desplazamiento < -config.max_amp:
            desplazamiento = -config.max_amp

        sound[nsample] = int(desplazamiento)

    return sound


def trim(sound, reduction: int, start: bool):  # Método que elimina cierto número de muestras

    if reduction >= len(sound):
        return []

    elif start == True:  # Quitar muestras del principio

        del sound[:reduction]
        return sound

    else:
        del sound[-reduction:]  # Quitar muestras del final
        return sound


def repeat(sound, factor: int):  # Método que repite la señal un número de veces (multiplica)

    if factor > 0:

        sound *= factor
        return sound

    else:
        return sound


def clean(sound, level: int):  # Método que reduce a 0 las muestras menores en valor absoluto

    for nsample in range(len(sound)):

        if sound[nsample] > level or sound[nsample] < -level:  # Representación del valor absoluto (abs)
            sound[nsample] = sound[nsample]

        else:
            sound[nsample] = 0

    return sound


def round(sound):  # Método que calcula la media de tres valores (redondea)

    lista = []

    for nsample in range(len(sound)):

        if nsample < 1:

            lista.append(sound[nsample])

        elif nsample >= 1 and nsample < (len(sound) - 1):

            a = sound[nsample - 1]
            b = sound[nsample]
            c = sound[nsample + 1]

            media = (a + b + c) // 3
            lista.append(int(media))

        elif nsample >= (len(sound) - 1):

            lista.append(sound[nsample])

    sound = lista

    return sound


def add(sound, sound2):  # Método que suma los mismos indices de dos listas dadas

    total = []

    if len(sound) > len(sound2):
        sound_menor = sound2
        sound_mayor = sound
    else:
        sound_menor = sound
        sound_mayor = sound2

    for nsamples in range(len(sound_menor)):

        suma = sound_menor[nsamples] + sound_mayor[nsamples]

        if suma > config.max_amp:
            suma = config.max_amp

        elif suma < -config.max_amp:
            suma = -config.max_amp

        total.append(suma)

        sound = total + sound_mayor[len(sound_menor):]

    return sound
