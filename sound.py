"""Programa que acepta como argumentos una fuente, un procesador y un sumidero"""

import sys

import sources
import sinks
import processors


def main():

    source = sys.argv[1]
    source_arg1 = sys.argv[2]

    # Fuentes:

    if source == 'sin':

        source_arg2 = sys.argv[3]
        sound = sources.sin(nsamples=int(source_arg1), freq=float(source_arg2))
        x = 3

    elif source == 'constant':

        source_arg2 = sys.argv[3]
        sound = sources.constant(nsamples=int(source_arg1), level=int(source_arg2))
        x = 3

    elif source == 'square':

        source_arg2 = sys.argv[3]
        sound = sources.square(nsamples=int(source_arg1), nperiod=int(source_arg2))
        x = 3

    elif source == 'load':

        sound = sources.load(path=source_arg1)
        x = 2

    else:

        sys.exit('ERROR')

    # Procesos:

    processor = sys.argv[x + 1]

    if processor == 'ampli':

        processor_arg1 = sys.argv[x + 2]
        processors.ampli(sound, factor=float(processor_arg1))
        x += 2

    elif processor == 'shift':

        processor_arg1 = sys.argv[x + 2]
        processors.shift(sound, value=int(processor_arg1))
        x += 2

    elif processor == 'trim':

        processor_arg1 = sys.argv[x + 2]
        processor_arg2 = eval(sys.argv[x + 3])
        processors.trim(sound, reduction=int(processor_arg1), start=bool(processor_arg2))
        x += 3

    elif processor == 'repeat':

        processor_arg1 = sys.argv[x + 2]
        processors.repeat(sound, factor=int(processor_arg1))
        x += 2

    elif processor == 'clean':
        processor_arg1 = sys.argv[x + 2]
        processors.clean(sound, level=int(processor_arg1))
        x += 2

    elif processor == 'round':
        processors.round(sound)
        x += 1

    elif processor == 'add':

        lista_add = []
        incremento = 1
        es = True

        while es:
            incremento += 1
            processor_arg1 = sys.argv[x + incremento]

            if processor_arg1 != 'show' and processor_arg1 != 'play' and processor_arg1 != 'draw' and processor_arg1 != 'info':
                lista_add.append(int(processor_arg1))

            else:
                es = False

        sound = processors.add(sound, sound2=lista_add)
        x += incremento - 1

    # Sumideros:

    sink = sys.argv[x + 1]
    if sink == 'play':

        sinks.play(sound)

    elif sink == 'draw':

        sink_arg1 = sys.argv[x + 2]
        sinks.draw(sound, max_chars=int(sink_arg1))

    elif sink == 'show':

        sink_arg1 = eval(sys.argv[x + 2])
        sinks.show(sound, newline=bool(sink_arg1))

    elif sink == 'info':

        sinks.info(sound)


if __name__ == '__main__':
    main()
