"""Programa menú que acepta una fuente, un procesador y un sumidero (funcionamiento similar a menu.py, sin
mensajes de error ni ejecución) """

import sources
import sinks
import processors


def main():

    # Fuentes:

    x = 0
    while x == 0:
        source = input("Dime una fuente (load, sin, constant, square): ")

        if source == 'sin':
            nsamples1 = input('nº samples: ')
            freq = int(input('frecuencia: '))

            sound = sources.sin(int(nsamples1), int(freq))
            x = 1

        elif source == 'constant':

            nsamples2 = input('nº samples: ')
            level = int(input('nivel para constant: '))

            sound = sources.constant(int(nsamples2), int(level))
            x = 1

        elif source == 'square':

            nsamples3 = int(input('nº samples: '))
            nperiod = int(input('periodo: '))

            sound = sources.square(int(nsamples3), int(nperiod))
            x = 1

        elif source == 'load':

            str =input('Dime el string para load (dirección del archivo): ')

            sound = sources.load(str)
            x = 1

    # Procesos:

    while x == 1:

        processor = input(f'Dime un procesador (ampli, shift, trim, repeat, clean, round, add): ')

        if processor == 'ampli':

            processor_arg1 = float(input('Introduce un factor para ampliar: '))
            processors.ampli(sound, factor=processor_arg1)
            x = 2

        elif processor == 'shift':

            processor_arg1 = int(input('Introduce un valor de desplazamiento: '))
            processors.shift(sound, value=processor_arg1)
            x = 2

        elif processor == 'trim':

            processor_arg1 = int(input('Introduce un valor de reducción: '))
            processor_arg2 = input(f'Introduce "True" para quitar muestras del principio, o "False" para '
                                   f'quitar valores del final: ')
            processors.trim(sound, reduction=int(processor_arg1), start=bool(processor_arg2))
            x = 2

        elif processor == 'repeat':

            processor_arg1 = int(input('Introduce un valor de repetición: '))
            processors.repeat(sound, factor=int(processor_arg1))
            x = 2

        elif processor == 'clean':
            processor_arg1 = int(input('Introduce un valor para "limpiar": '))
            processors.clean(sound, level=int(processor_arg1))
            x = 2

        elif processor == 'round':
            sound = processors.round(sound)
            x = 2

        elif processor == 'add':

            seguir = True
            lista_nueva = []

            processor_arg1 = int(input('Introduce enteros para crear una lista: '))
            lista_nueva.append(processor_arg1)

            while seguir:
                continuar = input('Desea seguir metiendo valores? (si) (no) ')

                if continuar == 'si':
                    processor_arg1 = int(input('Introduce enteros para crear una lista: '))
                    lista_nueva.append(processor_arg1)

                if continuar == 'no':
                    seguir = False

            sound = processors.add(sound, sound2=lista_nueva)

            x = 2

        # Sumideros:

    while x == 2:

        sink = input('Dime un sumidero (play, draw, show, info): ')

        if sink == 'play':

            sinks.play(sound)
            x = 0

        elif sink == 'draw':

            maximo_caracter = int(input('Numero máximo: '))  # no poner números pequeños
            sinks.draw(sound, maximo_caracter)

            x = 0

        elif sink == 'show':

            newline_ask = input('¿Números con salto de linea? (si - no): ')

            if newline_ask == 'si':
                sinks.show(sound, newline=True)

            else:
                sinks.show(sound, newline=False)

            x = 0

        elif sink == 'info':

            sinks.info(sound)

            x = 0


if __name__ == '__main__':
    main()
