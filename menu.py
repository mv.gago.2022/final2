"""Programa menú que acepta una fuente y un sumidero """

import sources
import sinks


def main():

    # Bucle while para la ejecución correcta del mensaje "Ejecutando ..."

    x = 0

    while x == 0:

        source = input("Dime una fuente (load, sin, constant, square): ")

        if source == 'sin':

            nsamples1 = input('nsamples : ')
            freq = float(input('frecuencia: '))

            sound = sources.sin(int(nsamples1), int(freq))
            x = 1

        elif source == 'constant':

            nsamples2 = input('nsamples: ')
            level = int(input('nivel para constant: '))

            sound = sources.constant(int(nsamples2), int(level))
            x = 1

        elif source == 'square':

            nsamples3 = int(input('nsamples: '))
            nperiod = int(input('Escribe el periodo: '))

            sound = sources.square(int(nsamples3), int(nperiod))
            x = 1

        elif source == 'load':

            str = input('Dime el string para load (dirección del archivo): ')

            sound = sources.load(str)
            x = 1

    # Mensaje de ejecución:

    while x == 1:

        sink = input('Dime un sumidero (play, draw, show, info): ')

        if source == 'sin' and (sink == 'play' or sink == 'draw' or sink == 'show' or sink == 'info'):
            print(f'Ejecutando sin({nsamples1} {freq}) y {sink}. Veamos los parámetros: ')

        elif source == 'constant' and (sink == 'play' or sink == 'draw' or sink == 'show' or sink == 'info'):
            print(f'Ejecutando constant({nsamples2} {level}) y {sink}. Veamos los parámetros: ')

        elif source == 'square' and (sink == 'play' or sink == 'draw' or sink == 'show' or sink == 'info'):
            print(f'Ejecutando square({nsamples3} {nperiod}) y {sink}. Veamos los parámetros: ')

        elif source == 'load' and (sink == 'play' or sink == 'draw' or sink == 'show' or sink == 'info'):
            print(f'Ejecutando load y {sink}. Veamos los parámetros: ')

    # Sumideros:

        if sink == 'play':

            sinks.play(sound)
            x = 0

        elif sink == 'draw':

            maximo_caracter = int(input('Numero maximo: '))  # no poner números pequeños
            sinks.draw(sound, maximo_caracter)

            x = 0

        elif sink == 'show':

            newline_ask = input('¿Números con salto de linea? (si - no): ')

            if newline_ask == 'si':
                sinks.show(sound, newline=True)

            else:
                sinks.show(sound, newline=False)

            x = 0

        elif sink == 'info':

            sinks.info(sound)

            x = 0


if __name__ == '__main__':
    main()
