"""Methods for producing sound (sound sources)"""

import math
import soundfile
import config


def load(path: str):
    """Load sound from a file

    :param path: path of the file to read
    :return:     list of samples (sound)
    """

    data, fs = soundfile.read(path, dtype='int16')

    # Create q liwt of integers
    sound = [0] * len(data)
    # Convert list of int16 to list of int
    for nsample in range(len(data)):
        sound[nsample] = data[nsample][0]

    return sound


def sin(nsamples: int, freq: float):
    """Produce a list of samples of a sinusoidal signal (sound)

    :param nsamples: number of samples
    :param freq:     frequency of the signal, in Hz (cycles/sec)
    :return:         list of samples (sound)
    """

    # Create q list of integers
    sound = [0] * nsamples

    # Compute sin samples (maximum amplitude, freq frequency)
    for nsample in range(nsamples):
        t = nsample / config.samples_second
        sound[nsample] = int(config.max_amp *
                             math.sin(2 * config.pi * freq * t))

    return sound


def constant(nsamples: int, level: int):

    sound = [0] * nsamples

    for nsamples in range(len(sound)):
        sound[nsamples] = level

        if level > config.max_amp:
            sound[nsamples] = config.max_amp

        elif level < -config.max_amp:
            sound[nsamples] = -config.max_amp

    return sound


def square(nsamples: int, nperiod: int):

    sound = [0] * nsamples
    p = 1

    for i in range(nsamples):

        if i % (nperiod/2) == 0:
            p *= -1
        sound[i] = p * config.max_amp

    return sound
