"""Programa menú que acepta fuente y sumidero, además de mensajes de error"""

import sources
import sinks


def main():

    # Sumideros

    x = 0
    while x == 0:
        source = input("Dime una fuente (load, sin, constant, square): ")

        if source == 'sin':

            respuesta_correcta = False
            print('Veamos los valores para sin ')

            while not respuesta_correcta:
                try:

                    nsamples1 = int(input('nº samples: '))
                    freq = int(input('frecuencia: '))

                    sound = sources.sin(int(nsamples1), int(freq))
                    respuesta_correcta = True

                except:
                    print(f'ERROR: Alguno de los valores introducidos no es un numero entero, escribir nuevamente... ')

            x = 1

        elif source == 'constant':

            respuesta_correcta = False
            print('Veamos los valores para constant')

            while not respuesta_correcta:
                try:

                    nsamples2 = int(input('nº samples: '))
                    level = int(input('nivel para constant: '))

                    sound = sources.constant(int(nsamples2), int(level))
                    respuesta_correcta = True

                except:
                    print(f'ERROR: Alguno de los valores introducidos no es un numero entero, escribir nuevamente... ')

            x = 1

        elif source == 'square':

            respuesta_correcta = False
            print('Veamos los valores para square')

            while not respuesta_correcta:
                try:

                    nsamples3 = int(input('nº samples: '))
                    nperiod = int(input('periodo: '))

                    sound = sources.square(int(nsamples3), int(nperiod))

                    respuesta_correcta = True

                except:
                    print(f'ERROR: Alguno de los valores introducidos no es un numero entero, escribir nuevamente... ')

            x = 1

        elif source == 'load':

            respuesta_correcta = False
            print('Veamos el valor para load')

            while not respuesta_correcta:
                try:

                    str = input('dirección del archivo: ')

                    sound = sources.load(str)
                    respuesta_correcta = True

                except:
                    print(f'ERROR: El valor introducido no es un string .wav o no esta en archivos'
                          f', escribir nuevamente... ')

            x = 1

    # Mensaje de ejecución

    while x == 1:

        sink = input('Dime un sumidero (play, draw, show, info): ')

        if source == 'sin'and (sink == 'play' or sink == 'draw' or sink == 'show' or sink == 'info'):
            print(f'Ejecutando sin({nsamples1} {freq}) y {sink}. Veamos los parámetros: ')

        elif source == 'constant'and (sink == 'play' or sink == 'draw' or sink == 'show' or sink == 'info'):
            print(f'Ejecutando constant({nsamples2} {level}) y {sink}. Veamos los parámetros: ')

        elif source == 'square'and (sink == 'play' or sink == 'draw' or sink == 'show' or sink == 'info'):
            print(f'Ejecutando square({nsamples3} {nperiod}) y {sink}. Veamos los parámetros: ')

        elif source == 'load'and (sink == 'play' or sink == 'draw' or sink == 'show' or sink == 'info'):
            print(f'Ejecutando load y {sink}. Veamos los parámetros: ')

        # Sumideros:

        if sink == 'play':

            sinks.play(sound)

            x = 0

        elif sink == 'draw':

            respuesta_correcta = False

            while not respuesta_correcta:
                try:

                    maximo_caracter = int(input('Numero maximo: '))  # no poner números pequeños
                    sinks.draw(sound, maximo_caracter)

                    respuesta_correcta = True

                except:
                    print(f'ERROR: El valor introducido no es un numero entero, escribir nuevamente... ')

            x = 0

        elif sink == 'show':

            respuesta_correcta = False

            while not respuesta_correcta:

                newline_ask = input('¿Números con salto de linea? (si - no): ')

                if newline_ask == 'si':
                    sinks.show(sound, newline=True)
                    respuesta_correcta = True

                elif newline_ask == 'no':
                    sinks.show(sound, newline=False)
                    respuesta_correcta = True

                else:
                    print(f'ERROR: El valor introducido no es si o no, escribir nuevamente... ')

            x = 0

        elif sink == 'info':

            sinks.info(sound)

            x = 0


if __name__ == '__main__':
    main()
